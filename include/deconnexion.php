<?php

  if(isset($_POST['deconnexion']))
  {
    if (isset($_SESSION['email'],$_SESSION['idUtilisateur']))
    {
      session_unset();
      session_destroy();
      echo "deconnexion";
    }
  }

  header('location: ../index.php');
  exit;
?>
