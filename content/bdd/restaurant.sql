

DROP DATABASE restaurant20;
CREATE DATABASE restaurant20
CREATE TABLE utilisateur
(
    idUtilisateur INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    prenom VARCHAR(100),
    email VARCHAR(255),
    statut INT NOT NULL DEFAULT 0,
    mdp VARCHAR(100)
);
CREATE TABLE restaurant
(
    idRestaurant INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    image1 VARCHAR(255),
    image2 VARCHAR(255)
);

CREATE table salade
(
    idSalade INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255)
);
CREATE table entree
(
    idEntree INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255)
);
CREATE table plat
(
    idPlat INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255)
);
CREATE table dessert
(
    idDessert INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255)
);

CREATE table boisson
(
    idBoisson INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255)
);
CREATE table region
(
    idRegion INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100)
);
CREATE table avis
(
    idAvis INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    description TEXT(500),
    note INT,
    dateAvis DATE
);
CREATE table vin
(
    idVin INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    image VARCHAR(255),
    idregion INT,
    CONSTRAINT fk_idRegion   -- On donne un nom à notre clé
        FOREIGN KEY (idRegion)   -- Colonne sur laquelle on crée la clé
        REFERENCES region(idRegion)  -- Colonne de référence

);
CREATE table formule
(
    idFormule INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    description TEXT(500),
    prix INT,
    statut BOOLEAN NOT NULL,
    idEntree INT,
    idPlat INT,
    idDessert INT,
    CONSTRAINT fk_idEntree
        FOREIGN KEY (idEntree)
        REFERENCES entree(idEntree),
    CONSTRAINT fk_idPlat
        FOREIGN KEY (idPlat)
        REFERENCES plat(idPlat),
    CONSTRAINT fk_idDessert
        FOREIGN KEY (idDessert)
        REFERENCES dessert(idDessert)

);

CREATE table avisRestaurant
(
    idAvis INT NOT NULL,
    idRestaurant INT NOT NULL,
    PRIMARY KEY (idAvis,idRestaurant),
    CONSTRAINT fk_idAvisRestaurant   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idRestaurant   -- On donne un nom à notre clé
        FOREIGN KEY (idRestaurant)   -- Colonne sur laquelle on crée la clé
        REFERENCES restaurant(idRestaurant)  -- Colonne de référence
);
CREATE table avisFormule
(
    idAvis INT NOT NULL,
    idFormule INT NOT NULL,
    PRIMARY KEY (idAvis,idFormule),
    CONSTRAINT fk_idAvisFormule   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idFormule   -- On donne un nom à notre clé
        FOREIGN KEY (idFormule)   -- Colonne sur laquelle on crée la clé
        REFERENCES formule(idFormule)  -- Colonne de référence
);
CREATE table avisSalade
(
    idAvis INT NOT NULL,
    idSalade INT NOT NULL,
    PRIMARY KEY (idAvis,idSalade),
    CONSTRAINT fk_idAvisSalade   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idSalade   -- On donne un nom à notre clé
        FOREIGN KEY (idSalade)   -- Colonne sur laquelle on crée la clé
        REFERENCES salade(idSalade)  -- Colonne de référence
);
CREATE table avisEntree
(
    idAvis INT NOT NULL,
    idEntree INT NOT NULL,
    PRIMARY KEY (idAvis,idEntree),
    CONSTRAINT fk_idAvisEntree   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idEntreeAvis   -- On donne un nom à notre clé
        FOREIGN KEY (idEntree)   -- Colonne sur laquelle on crée la clé
        REFERENCES entree(idEntree)  -- Colonne de référence
);
CREATE table avisPlat
(
    idAvis INT NOT NULL,
    idPlat INT NOT NULL,
    PRIMARY KEY (idAvis,idPlat),
    CONSTRAINT fk_idAvisPlat   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idPlatAvis   -- On donne un nom à notre clé
        FOREIGN KEY (idPlat)   -- Colonne sur laquelle on crée la clé
        REFERENCES plat(idPlat)  -- Colonne de référence
);
CREATE table avisDessert
(
    idAvis INT NOT NULL,
    idDessert INT NOT NULL,
    PRIMARY KEY (idAvis,idDessert),
    CONSTRAINT fk_idAvisDessert   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idDessertAvis   -- On donne un nom à notre clé
        FOREIGN KEY (idDessert)   -- Colonne sur laquelle on crée la clé
        REFERENCES dessert(idDessert)  -- Colonne de référence
);
CREATE table avisVin
(
    idAvis INT NOT NULL,
    idVin INT NOT NULL,
    PRIMARY KEY (idAvis,idVin),
    CONSTRAINT fk_idAvisVin   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idVin   -- On donne un nom à notre clé
        FOREIGN KEY (idVin)   -- Colonne sur laquelle on crée la clé
        REFERENCES vin(idVin)  -- Colonne de référence
);
CREATE table avisBoisson
(
    idAvis INT NOT NULL,
    idBoisson INT NOT NULL,
    PRIMARY KEY (idAvis,idBoisson),
    CONSTRAINT fk_idAvisBoisson   -- On donne un nom à notre clé
        FOREIGN KEY (idAvis)   -- Colonne sur laquelle on crée la clé
        REFERENCES avis(idAvis),  -- Colonne de référence
    CONSTRAINT fk_idBoisson   -- On donne un nom à notre clé
        FOREIGN KEY (idBoisson)   -- Colonne sur laquelle on crée la clé
        REFERENCES boisson(idBoisson)  -- Colonne de référence
)
