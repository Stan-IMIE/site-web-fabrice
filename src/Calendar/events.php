<?php
namespace Calendar;

/**
 * [Events description]
 */
class Events{

  /**
   * Permmet de recuper les evenement commencant entre deux dates
   * @param  Date  $start [description]
   * @param  Date  $end   [description]
   * @return array        [description]
   */
  public function getEventBeteween(\Datetime $start, \Datetime $end): array{
    $dbh = new \PDO('mysql:host=localhost;dbname=shiatsu_angers', "root", "");
    $sql = "SELECT * FROM filoshiatsu WHERE start_date and end_date";
    $requete = $dbh->prepare($sql);
    $requete->execute();
    $result_date = $requete->fetchAll();
    return $result_date;
  }

  /**
   * Récuper les evenement mais indexer par leur jour de début
   * @param  Datetime $Start [description]
   * @param  Datetime $end   [description]
   * @return array           [description]
   */
  public function getEventsByStartDay(\Datetime $start, \Datetime $end):array{
    $events = $this->getEventBeteween($start,$end);
    $days = [];
    foreach($events as $event){
      $date = explode(' ',$event['start_date'])[0];
      if (!isset($days[$date])) {
        $days[$date] = [$event];
      }else {
        $days[$date][] = $event;
      }
    }
    return $days;
  }



  /**
   * Récuper les evenement mais indexer par leur jour de fin
   * @param  Datetime $Start [description]
   * @param  Datetime $end   [description]
   * @return array           [description]
   */
  public function getEventsByEndDay(\Datetime $start, \Datetime $end):array{
    $events = $this->getEventBeteween($start,$end);
    $days = [];
    foreach($events as $event){
      $date = explode(' ',$event['end_date'])[0];
      if (!isset($days[$date])) {
        $days[$date] = [$event];
      }else {
        $days[$date][] = $event;
      }
    }
    return $days;
  }






}//end Class

?>
