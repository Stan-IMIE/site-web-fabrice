
<div class="block-main">
  <div class="inner">
    <input type="text" name="testouill" value="">
    <?php
    // vérification de l'article à modify
    $id_modify = @$_GET["article"];
    var_dump(empty($id_modify));

    if (empty($id_modify) == false) {
      $sql = "SELECT title, content, FK_category, FK_adminUser FROM post WHERE id_post = '$id_modify'";
      $requete = $dbh->prepare($sql);
      $requete->execute();
      $post_modify = $requete->fetchAll(PDO::FETCH_ASSOC);
    }
    ?>

    <div class="lien_page_principale">
      <a href="http://localhost/site-fabrice/index.php"><button type="button" name="button">Page Principale</button></a>
    </div>

    <?php
    // check session admin
    require_once('include/config.php');
    // session_start();
    if ($_SESSION['isAdmin']) {
      echo "Welcome " . $_SESSION['authUser']." ".$_SESSION['id'];
    }else {
      echo "Get out you're not authorized";
    }

    if(!isset($_SESSION["isAdmin"]) || (isset($_SESSION["isAdmin"]) && !$_SESSION["isAdmin"])) {
      echo "Unauthorized Access";
      exit;
    }
    ?>

    <?php
    // Requette qui permet de vérifier les dates dispo
    $sql = "SELECT contenue_filo, start_date, end_date FROM filoshiatsu WHERE start_date IS NOT NULL && end_date IS NOT NULL";
    $requete = $dbh->prepare($sql);
    $requete->execute();
    $les_dates = $requete->fetchAll(PDO::FETCH_ASSOC);

    ?>
    <script>
    var variableRecuperee = <?php echo json_encode($les_dates); ?>;
    //Récupération Saisie utilisateur + comparaison date éxistante
    function myFunction_debut() {
      var debut = document.querySelector("#datepicker_debut").value;
      for (var i in variableRecuperee) {
        if (debut > variableRecuperee[i].end_date) {
          console.log("Date acceptable");
        }else{
          console.log("Date égale à la BDD donc refusser");
        }
      }
    }

    </script>



    <?php
    // ------------------------
    // SCRIPT CATEGORIE
    // -----------------------
    $categories = [];
    if (isset($_POST["name"])) {
      $sql = "INSERT INTO categories(name) VALUES (:name);";
      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':name', $_POST["name"]);
      $stmt->execute();
    }

    $sql = "SELECT id ,name FROM categories;";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (isset($_POST["name"])){
      echo json_encode($categories);
      return;
    }

    ?>


    <h1>Rédaction d'un article : </h1>

    <div>
      <div>

        <div>Choisissez la catégorie :</div>
        <div>
          <select id="listCategories">
            <?php


            foreach ($categories as $category)
            {
              echo "<option ";

              if (isset($post_modify) && ($post_modify["0"]["FK_category"] == $category["id"])) {
                echo "selected";
              }

              echo " value='".$category["id"]."'>".$category["name"]."</option>";

            }
            ?>
          </select>
        </div>

        <div>
          Ajouter une nouvelle catégorie :
          <button id="addCategory">Créer une nouvelle catégorie</button>
        </div>

        <div class="">
          <span>Titre de l'article :</span>
          <input id="postTitle" type="text" name="title_article" value="<?php if(isset($post_modify)){echo $post_modify["0"]["title"];} ?> ">
        </div>
      </div>

      <div>
        <textarea name="editor" value=""><?php if(isset($post_modify)){echo $post_modify["0"]["content"];} ?></textarea>
      </div>

      <?php
      if(isset($post_modify) == FALSE ){
        echo
        "<p>Date Début: <input type=\"text\" id=\"datepicker_debut\" name=\"datepicker_debut\" onchange=\"myFunction_debut();\"></p>
        <p>Date Fin: <input type=\"text\" id=\"datepicker_fin\" name=\"datepicker_fin\"  ></p>"
        ;
      };
      ?>

    </div>

    <div>
      <?php if(isset($post_modify)){
        echo "<button id=\"modify\">Modifier</button>";
      }else{
        echo "<button id=\"publish\">Publier</button>";
      };?>
    </div>




    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
    <script>

    // -----------------------
    //   Création catégory
    // -----------------------
    CKEDITOR.replace( 'editor' );
    $('#addCategory').on('click', function(){
      var newName = prompt("Entrez le nom de votre catégorie :");


      $.ajax({
        method: "POST",
        data: {
          "name": newName
        },
        dataType: "json",
        success: function(categories){
          var categoriesHtml;
          for (category of categories){
            categoriesHtml += "<option value='" + category[0] + "'>" + category[1] +  "</option>";
          }
          $('#listCategories').html(categoriesHtml);
        }
      });
    });
    </script>



    <script>


    // ------------------------
    // SCRIPT ARTICLE
    // -----------------------
    $('#modify').on('click', function(){
      $.ajax({
        method: "POST",
        data: {
          "post": CKEDITOR.instances.editor.getData(),
          "title": $("#postTitle").val(),
          "category" : $("#listCategories").val()

        },
        success: function(){
          window.location.href = "/site-fabrice/index.php";

          <?php
          if (isset($_POST["title"]) && isset($_POST["post"])) {

            $sql = "UPDATE post SET title = :title,
            content = :content,
            FK_category = :categoryId
            WHERE id_post = '$id_modify'";

            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':title', $_POST['title']);
            $stmt->bindValue(':content', $_POST['post']);
            $stmt->bindValue(':categoryId', $_POST['category']);
            $stmt->execute();
            return;
          }
          ?>
        }
      })
    });

    </script>


    <script>

    $('#publish').on('click', function(){
      $.ajax({
        method: "POST",
        data: {
          "post": CKEDITOR.instances.editor.getData(),
          "title": $("#postTitle").val(),
          "category" : $("#listCategories").val(),
          "date_debut" : $("#datepicker_debut").val(),
          "date_fin" : $("#datepicker_fin").val()

        },
        success: function(){
          window.location.href = "/site-fabrice/index.php";

          <?php
          // ------------------------
          // SCRIPT Ajoute d'article en ligne
          // ------------------------
          //Si toute les conditions sont respecter execution de la requette pour ajouter le contenue à la BDD
          if (isset($_POST["title"]) && isset($_POST["post"]) && $_POST["category"] === "4") {

            $sql = "INSERT INTO filoshiatsu(titre_filo, contenue_filo, FK_category, FK_adminUser, start_date, end_date) VALUES (:title, :content, :categoryId, :authorId, :date_debut, :date_fin)";

            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':title', $_POST['title']);
            $stmt->bindValue(':content', $_POST['post']);
            $stmt->bindValue(':categoryId', $_POST['category']);
            $stmt->bindValue(':authorId', $_SESSION['id']);

            if ($_POST['date_debut'] == NULL) {
              $stmt->bindValue(':date_debut', NULL, PDO::PARAM_NULL);
            }else{
              $stmt->bindValue(':date_debut', $_POST['date_debut']);
            }

            if ($_POST['date_fin'] == NULL) {
              $stmt->bindValue(':date_fin', NULL, PDO::PARAM_NULL);
            }else{
              $stmt->bindValue(':date_fin', $_POST['date_fin']);
            }

            $stmt->execute();
            return;
          }elseif (isset($_POST["title"]) && isset($_POST["post"])) {

            $sql = "INSERT INTO post(title, content, FK_category, FK_adminUser) VALUES (:title, :content, :categoryId, :authorId)";

            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':title', $_POST['title']);
            $stmt->bindValue(':content', $_POST['post']);
            $stmt->bindValue(':categoryId', $_POST['category']);
            $stmt->bindValue(':authorId', $_SESSION['id']);
            $stmt->execute();
            return;
          }
          ?>

        }
      })
    });
    </script>



    <!-- **************************************************
    CALENDRIER
    ************************************************** -->
    <title>jQuery UI Datepicker - Default functionality</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $(function() {
      $("#datepicker_debut").datepicker({
        dateFormat: "yy-mm-dd"
      })});
      </script>
      <script>
      $(function() {
        $("#datepicker_fin").datepicker({
          dateFormat: 'yy-mm-dd'
        })});
        </script>

        <!-- *************************************************************************


        ***************************************************************************** -->
        <?php
        require 'src/Calendar/month.php';
        require 'src/Calendar/events.php';
        $events = new Calendar\Events();
        $bool_event = "false";
        ?>
        <div class="containeur_calandar">

          <?php


          //  for ($j=1; $j < 13; $j++) {

          $month = new App\Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null);
          $start = $month->getStartingDay();
          $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
          $weeks = $month->getWeeks();
          $end = (clone $start)->modify('+'.(6+7*($weeks - 1)).' days');
          $start_events = $events->getEventsByStartDay($start,$end);
          $end_events = $events->getEventsByEndDay($start,$end);



          // echo '<pre>';
          // print_r($start_events);
          // echo '</pre>';


          ?>

          <div class="calandar">

            <div class="calandar__titre__mois">
              <h1><?php echo $month->toString(); ?></h1>

            </div>



            <table class="calandar__table">
              <?php for ($i=0; $i < $month->getWeeks() ; $i++):?>
                <tr>



                  <?php
                  //Affichage du jour
                  foreach ($month->days as $k => $day):
                    //var_dump($month->days);
                    $date = (clone $start)->modify("+".($k+$i*7)."days");
                    //var_dump($start_events[$date->format('Y-m-d')]);
                    $StarteventForDay = $start_events[$date->format('Y-m-d')] ?? [];
                    $EndeventForDay = $end_events[$date->format('Y-m-d')] ?? [];
                    // echo '<pre>';
                    // print_r($EndeventForDay);
                    // echo '</pre>';

                    ?>
                    <td class="<?php echo $month->withinMonth($date)? '' : 'calandar__othermonth';?>">
                      <?php if ($i === 0):?>
                        <div class="calandar__weekday"><?= $day;?></div>
                      <?php endif ?>
                      <div class="calandar__day"><?= $date->format('Y-m-d') ?></div>
                      <?php// var_dump($StarteventForDay);?>
                      <?php

                      // detection de l'evenement
                      foreach ($StarteventForDay as $event_start):?>
                      <?php
                      // var_dump($event_start['end_date']);
                      // var_dump($date->format('Y-m-d'));
                      if ($event_start['start_date'] === $date->format('Y-m-d')) {
                        $bool_event = "true" ;
                        echo "<p class=\"calandar__event evenement ".$bool_event."\">Ok</p>" ;
                      }
                    endforeach;



                    foreach ($EndeventForDay as $event_end):
                      if($event_end['end_date'] === $date->format('Y-m-d')){
                        $bool_event = "false" ;
                        //return $bool_event;
                      }
                    endforeach;

                    if ($bool_event == "true") {
                      echo "<p class=\"calandar__event evenement\">Ok</p>" ;
                    }

                    ?>



                  </td>
                <?php endforeach;
                // fin affichage jour?>
              </tr>
            <?php endfor;?>
          </table>
        </div>

        <?php //};?>
        <script type="text/javascript">
          var
        </script>
      </div>
      <?php var_dump($date->format('Y-m-d')); ?>
