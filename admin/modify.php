<div class="block-main">
  <div class="inner">
    <?php $id_du_poste_modify = $_GET["article"];
    var_dump($id_du_poste_modify);
    ?>

    <div class="lien_page_principale">
      <a href="http://localhost/site-fabrice/index.php"><button type="button" name="button">Page Principale</button></a>
    </div>

    <?php
    require_once('include/config.php');
    // session_start();
    if ($_SESSION['isAdmin']) {
      echo "Welcome " . $_SESSION['authUser']." ".$_SESSION['id'];
    }else {
      echo "Get out you're not authorized";
    }

    if(!isset($_SESSION["isAdmin"]) || (isset($_SESSION["isAdmin"]) && !$_SESSION["isAdmin"])) {
      echo "Unauthorized Access";
      exit;


    }
    ?>




    <?php
    // Recupération des dattes présent dans la basse de donnée
    $sql = "SELECT start_date, end_date FROM post WHERE start_date and end_date IS NOT NULL and id_post != $id_du_poste_modify";
    $requete = $dbh->prepare($sql);
    $requete->execute();
    $les_dates = $requete->fetchAll(PDO::FETCH_ASSOC);

    ?>
    <script>
    // var variableRecuperee = <?php echo json_encode($les_dates); ?>;
    // //Récupération Saisie utilisateur + comparaison date éxistante
    // function myFunction_debut() {
    //   var debut = document.querySelector("#datepicker_debut").value;
    //   for (var i in variableRecuperee) {
    //     if (debut > variableRecuperee[i].end_date) {
    //       console.log("Date acceptable");
    //     }else{
    //       console.log("Date égale à la BDD donc refusser");
    //     }
    //   }
    // }

    </script>



    <?php
    // ------------------------
    // SCRIPT CATEGORIE
    // -----------------------
    $categories = [];
    if (isset($_POST["name"])) {
      $sql = "INSERT INTO categories(name) VALUES (:name);";
      $stmt = $dbh->prepare($sql);
      $stmt->bindValue(':name', $_POST["name"]);
      $stmt->execute();
    }

    $sql = "SELECT id ,name FROM categories;";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (isset($_POST["name"])){
      echo json_encode($categories);
      return;
    }

    ?>


    <h1>Rédaction d'un article : </h1>

    <div>
      <div>

        <div>Choisissez la catégorie :</div>

        <div>
          <select id="listCategories">
            <?php
            foreach ($categories as $category)
            {
              echo "<option value='".$category["id"]."'>".$category["name"]."</option>";
            }
            ?>
          </select>
        </div>

        <div>
          Ajouter une nouvelle catégorie :
          <button id="addCategory">Créer une nouvelle catégorie</button>
        </div>

        <div class="">
          <span>Titre de l'article :</span> <input id="postTitle" type="text" name="title_article" value="">
        </div>
      </div>

      <div>
        <textarea name="editor"></textarea>
      </div>

    </div>

    <div>
      <button id="modify" onclick="quandclick()">Publier</button>
    </div>



    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
    <script>

    // ------------------------
    // SCRIPT ARTICLE
    // -----------------------

    $('#modify').on('click', function(){
      $.ajax({
        method: "POST",
        data: {
          "post": CKEDITOR.instances.editor.getData(),
          "title": $("#postTitle").val(),
          "category" : $("#listCategories").val(),
          "date_debut" : $("#datepicker_debut").val(),
          "date_fin" : $("#datepicker_fin").val()

        },
        success: function(){
          // window.location.href = "/site-fabrice/admin/admin.php";

          <?php
          // ------------------------
          // SCRIPT Ajoute d'article en ligne
          // ------------------------
          //Si toute les conditions sont respecter execution de la requette pour ajouter le contenue à la BDD
          if (isset($_POST["title"]) && isset($_POST["post"])) {

            $sql = "INSERT INTO post(title, content, FK_category, FK_adminUser) VALUES (:title, :content, :categoryId, :authorId)";

            $stmt = $dbh->prepare($sql);
            $stmt->bindValue(':title', $_POST['title']);
            $stmt->bindValue(':content', $_POST['post']);
            $stmt->bindValue(':categoryId', $_POST['category']);
            $stmt->bindValue(':authorId', $_SESSION['id']);
            $stmt->execute();
            return;
          }
          ?>

        }
      })
    });
    </script>



    <!-- **************************************************
    CALENDRIER
    ************************************************** -->
    <!-- <title>jQuery UI Datepicker - Default functionality</title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $(function() {
      $("#datepicker_debut").datepicker({
        dateFormat: "yy-mm-dd"
      })});
      </script>
      <script>
      $(function() {
        $("#datepicker_fin").datepicker({
          dateFormat: 'yy-mm-dd'
        })});
        </script> -->

      </div><!-- Fin block inner -->
    </div><!-- Fin block main -->
